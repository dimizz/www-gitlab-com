---
layout: handbook-page-toc
title: Learning & Development
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Team Member Resources

The Learning & Development team has a number of resources to help team members learn new skills. Click on the buttons below to learn more about each opportunity:

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/people-group/learning-and-development/manager-development/" class="btn btn-purple" style="width:200px;margin:5px;">Transitioning to a Manager Role</a>
    <a href="/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/" class="btn btn-purple" style="width:200px;margin:5px;">Growth & Development Benefit</a>
    <a href="/handbook/people-group/learning-and-development/career-development/" class="btn btn-purple" style="width:200px;margin:5px;">Career Development & Mobility</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/people-group/learning-and-development/manager-challenge/" class="btn btn-purple" style="width:200px;margin:5px;">Manager Challenge</a>
    <a href="/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/" class="btn btn-purple" style="width:200px;margin:5px;">Leadership Chats</a>
    <a href="/handbook/people-group/learning-and-development/manager-development/" class="btn btn-purple" style="width:200px;margin:5px;">New Manager Resources</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/people-group/learning-and-development/gitlab-learn/" class="btn btn-purple" style="width:200px;margin:5px;">GitLab Learn</a>
    <a href="/handbook/people-group/learning-and-development/linkedin-learning/" class="btn btn-purple" style="width:200px;margin:5px;">LinkedIn Learning</a>
    <a href="/handbook/people-group/learning-and-development/compliance-courses" class="btn btn-purple" style="width:200px;margin:5px;">Compliance Courses</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/people-group/learning-and-development/mentor/" class="btn btn-purple" style="width:200px;margin:5px;">Mentorship</a>
    <a href="/handbook/leadership/coaching/" class="btn btn-purple" style="width:200px;margin:5px;">Coaching</a>
    <a href="/handbook/people-group/learning-and-development/learning-initiatives/slack-training/" class="btn btn-purple" style="width:200px;margin:5px;">Slack Training</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <a href="/handbook/sales/field-manager-development/" class="btn btn-purple" style="width:200px;margin:5px;">Field Manager Development</a>
    <a href="/handbook/people-group/learning-and-development/learning-initiatives/" class="btn btn-purple" style="width:200px;margin:5px;">Learning Initiatives</a>
    <a href="/handbook/leadership/life-labs-learning/" class="btn btn-purple" style="width:200px;margin:5px;">LifeLabs Learning</a>
  </div>
</div>


## Overview

### Purpose

_Why we exist_

L&D is here to guide team members on their career journey by maintaining a culture of learning and development so that GitLab is a great place to work. We ensure team members can do their jobs so that GitLab can achieve results that make our customers and people very happy. 

### Vision 

_Where are we going_

A future where GitLab is best in class organization for remote learning & development, recognized by industry leaders. A future where **everyone contributes** to a culture of curiosity driven by team members. 

### Mission

_What we do_

We equip and empower individual contributors and people leaders to access self-service learning that role models a culture of development. We do this by: 

- Focusing on skill-based learning 
- Enabling access to resources to improve career mobility 
- Developing learning journeys for teams throughout the organization

### Objectives

_Performance indicators_ 

1. Reduce voluntary attrition by 5% each year by measuring [exit survey data](/handbook/people-group/offboarding/#exit-survey) related to career development
2. Increase in 5% each year for the Growth & Development category of the [annual engagement survey](/handbook/people-group/engagement/)
3. Increase in access to career mobility: internal transfers, [promotions](/handbook/people-group/promotions-transfers/), job shadows, intern for learning, and professional development
4. Evidence of best in class remote learning & development (i.e. [Brandon Hall Awards](https://www.brandonhall.com/excellenceawards/index.html), Industry Speaking Events, Thought Leadership articles, etc.)

### Principles

1. **Meaningful and relevant content.** We deliver learning solutions that drive team member's development and growth to reach professional goals.

1. **Values aligned.** Our learning solutions reinforce GitLab’s [values](/handbook/values/), and foster continuous learning and curiosity.

1. **Diverse approaches to learning.** We apply a blended learning model for learning solutions that adapt to various learning needs.

1. **Community.** We make our L&D offerings available to the public, aligned to our mission that everyone can contribute.

### What are we not responsible for

1. Customer & Product Training
2. Designing, developing, and delivering all training to team members


## GitLab Learning Strategy & Roadmap

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSfTwBDgrL07yg9Zpk08CQ1U52ZpvaYN5qB9YsSphS55-AbRYAmJBeCb0ClN-Cb4-SKKObk8pnTXLhD/embed?start=true&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

**Learn more about our [FY22 Roadmap and tracking sheet](https://docs.google.com/spreadsheets/d/1DuJ_gWqh0oWyMkMPHKlFs-7eBX98g7oD3QUQfPPzHtQ/edit#gid=1557718375)**.

## FY23 Learning & Development Calendar

<figure class="video_container">
<iframe src="https://calendar.google.com/calendar/embed?src=c_glhi0qhc8hs9fp4h9id2hc7lfg%40group.calendar.google.com&ctz=America%2FNew_York" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

**Note: Learning & Development activities are subject to change**

Internally, the GitLab People Group uses a calendar to track cross-funcational efforts. Please see the [People Group Page](/handbook/people-group/#team-calendars) for more information.

## L&D Organization

We are a small team, but we've got a big role to play at GitLab! 

* [Learning and Development Manager](/job-families/people-ops/learning-development/#learning--development-manager): 
* [Learning and Development Program Manager](https://about.gitlab.com/job-families/people-ops/learning-development/#intermediate-learning--development-program-manager): [Jacie Bandur](/company/team/#jbandur)
* [Learning and Development Program Manager](https://about.gitlab.com/job-families/people-ops/learning-development/#intermediate-learning--development-program-manager): [Samantha Lee](/company/team/#slee24)
* [Instructional Designer](/job-families/people-ops/instructional-designer/): [Irina Grosu](https://about.gitlab.com/company/team/#irinagrosu)
* [Learning System Administrator](/job-families/people-ops/learning-system-administrator/): [Jamie Allen](https://about.gitlab.com/company/team/#jallen16)

## Contact Us

Slack: `#learninganddevelopment`

Email Us: `learning@gitlab.com`

[Request support/Collaborate on new learning content](/handbook/people-group/learning-and-development/work-with-us)

Please take 5 minutes for a survey to [tell us how do you prefer to learn](https://forms.gle/Hafkqygy6LfJxWFw6)!


## Work With Us

To request support from the L&D, please see the process for [how to work with us](/handbook/people-group/learning-and-development/work-with-us). This page outlines issue templates and processes that are managed on our [Learning and Development issue board](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/issues) for support using the following templates:

Our team will review and set the priority for your request based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. 

We prioritize requests using this criteria:

*  Evaluate the strategic impact of the learning session
*  Determine the level of work associated with the learning requirement
*  Assess the impacted audience groups of the session 
*  Identify measures of success 
*  Assess dates of delivery with course schedule and forecast future date

## How we work

### Handbook First Training Content

All material in the handbook is considered training. The Learning & Development team pulls content from the handbook to build [handbook first learning content](/handbook/people-group/learning-and-development/interactive-learning). One of L&D's primary responsibilities is to ensure that content lives in the appropriate section in the handbook. In the below video, Sid, explains how the content of courses is not separated from the handbook to the L&D team. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/G57Wtt7M_no" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Learning & Development Team Meetings

Overview of the type of meetings the Learning & Development participates in every quarter:

| Meeting Type | Cadence/Time | Description |
| ------ | ------ | ------ |
| 1:1 | Weekly meeting/25 minutes | <br> - 1:1 meeting between direct report and manager of L&D team. <br> - Topics covered include: task management, career development, performance management, blockers, project updates, brainstorming, coaching, etc. <br> - During the meeting, if their is an item that requires broader team collaboration it will be added to the bi-weekly team meeting. |
| L&D Team Meeting | Every two weeks/40 minutes | <br> - Strategic items that have been brought up during 1:1 meetings that impact the team as a whole. <br> - The meetings will cover items that are strategic in nature that require discussion, brainstorming, collaboration, and synthesis with the team |
| L&D Quarterly Strategic Meeting | Quarterly/90 minutes to 2 hours | <br> - Review what was acheived in the quarter, what wasn't, and lessons learned <br> - Team members will present plans on what they are focused on for the next quarter <br> - Review team morale, team dynamics, learner satisfaction, long term impact, and alignment with Peope Group goals. |
 
### L&D team sprints

The L&D team uses GitLab issue boards to track priorities, organize collaboration, and set due dates based on three-week sprints.

* The L&D team uses [this sprint issue board](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/boards/1958538) to track priority issues each quarter.
* The `open` list is a queue for issues that need to be addressed, but have not yet been assigned to a sprint, backlog, or priority.
* When a new issue is opened, it should be moved to either the `ld-backlog` or `ld-fyxx-q1-priority` list. This will determine if the issue is a priority for the current quarter, or a backlog issue to be addresses as time allows.
* The team maintains three milestones, one for each upcoming sprint. The sprints are organized with a milestone using the naming mechanism `L&D Sprint # (Date Range)`
* Issues can be moved from the `ld-backlog` or `ld-fyxx-q1-priority` list to the correct sprint when they are ready to be assigned/addressed.
* When an issue is closed, it should remain in the given milestone.
* At the end of the sprint, the milestone can be removed from the board, or the milestone list can be hidden on the issue board.
* The `L&D Requests` list should be used to organize requests for L&D support or courses coming from other teams.
* The `manager-challenge` list holds issues related to each Manager Challenge and can be hidden on the sprint board.
* The `advanced-software-engineering-course` list houses issues related to external software engineer courses can can be hidden on the sprint board.

Additional planning and notes around the sprint board can be found in [this issue.](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/111)

#### Best practices for using sprint boards and issues:

- Apply a burn-down chart with milestones to track time spent
- Create the issue in the sprint where the work starts
- When creating L&D content, apply one issue to one course when developing content. Maintain all course development material in the issue for organization. 
- Epics do not show up in the boards
- Apply a Storyboard template for course development
- Apply labels to manage different work and priorities (leadership requests, prioritized, p1-p3, triage, WIP, Backlog)
- Consider having GitLab team members vote on priority issues to determine need and interest in learning & development content
- Stack rank issues in the board based on priority if possible
- Use the Time Tracking feature on issues to track time spend. When opening a new issue, use the `/estimate` command to set an estimate for the project to be complete. After each working session, use the`/spend` command to track actual time spent.
- Consider using the `new-initiative` issue template when planning a new learning initiative, engagement program, or program idea
- Consider using the `content-scoping` issue template when proposing a new pathway, creating a new course, or building any new learning experience

### L&D handbook merge requests

If you want the wider L&D team to be aware of your MR, please apply the `ld-handbook-update` label to the MR. This will trigger a Zapier automation that posts the MR title and link to the #learning-team Slack channel.

## Developing Learning Content

### Top Five Training Content Development Principles

If you are developing content to meet your learning needs or partnering with the L&D team, here are five key principles to consider when formulating a learning session: 

1. **Know Your Audience** - Analyze and assess the course audience. Ensure that all audience needs are accounted for at every level in the organization you are delivering the training too. 

2. **Define Learning Objectives** - Highlight the learner outcome. Consider developing two to three broad overall statements of what the audience will achieve. 

3. **Break Down Complex Information** - Consider breaking down complex information into easy to digest visuals or text. Reference the handbook but do not be afraid to create a visual representation or use storytelling for the audience.

4. **Engage the Learner** - Adults learn through practice and involvement. Consider using tools to engage learners in a virtual setting like [Mentimeter](https://www.mentimeter.com/) or [Kahoot](https://kahoot.com/business-u/) to stimulate interactivity. Ask the [L&D team](/handbook/people-group/learning-and-development/) for more insight on learning engagement tools. There is a lot you can leverage! 

5. **Implement Blended Learning Course Content** - Give the audience some pre-course work to read and review before the learning session. Use off-the-shelf resources and ensure the content is applicable to what will be covered in the session. Follow up with the audience following the session to gauge how they've applied what they've learned on the job through surveys and questionnaires. 

### Application of Adult Learning Theory

Adults learn differently in the workplace than in traditional learning environments or how they learned growing up. If you are developing training, consider applying principles related to Adult Learning Theories, those include: 

1. **Transformative learning:** The learning experience should aim to change the individual through transformative learning approaches. Start with learning experiences that appeal to your specific audience, and then move to activities that challenge assumptions and points of view.   

2. **Self-directed learning:** Most of the learning that adults do is outside the context of formal training, so there should be an emphasis on augmenting those informal learning experiences. Infuse applications of pre-reads and post-course follow up. Have the participants bring up examples of self-directed learning that they have taken that is related to the training course. 

3. **Experiential learning:** Adults learn through experiences and by doing. When designing a learning experience, apply activities to stimulate learning by doing through role-playing, simulations, virtual labs, case studies, etc. 

4. **Andragogy:** Recognize that adults learn differently than children. Design learning experiences with the assumption that your participants will come to the table with their own set of life experiences and motivations. Adults tend to direct their own learning, tend to learn better by doing, and will want to apply their learning to concrete situations as soon as possible. 

### Developing Learning Objectives

If you are developing training, add learning objectives to the beginning of the content to state a clear training outcome. A clear learning objective describes what the learner will do upon completion of a learning/training activity. Good learning objectives are what you want team members to learn or achieve. 

**Steps to creating learning objectives:** 
1. Identify the level of knowledge necessary to achieve the aim of the training. Use [Bloom's Taxonomy](https://tips.uark.edu/using-blooms-taxonomy/) to assist with writing practical learning objectives. 
2. Select an [action verb](https://www.bu.edu/cme/forms/RSS_forms/tips_for_writing_objectives.pdf). 
3. Create your very own objective
4. Check your objective. Make sure it includes these four pieces: audience, behavior, condition, and degree of mastery
5. Repeat these steps for each objective

**Sample learning objectives:** 
- By the end of the session, team members will be able to describe the steps taken to address underperformance
- Team members will be able to apply the GROW coaching model framework to coaching sessions with members of their team 
- After learning about the high-performance team-building model, team members will be able to determine the steps needed to reach high performance.

### 70-20-10 Model for Learning and Development

The L&D team uses a formula to describe the optimal sources of learning at GitLab. It shows that team members obtain 70 percent of the knowledge from job-related experiences, 20 percent from interactions with others, and 10 percent from formal learning events. The model is intended to show that hands-on experience (70 percent) can be one of the most beneficial for team members because it enables them to discover and refine their job-related skills. Team members learn from others (20 percent) through a variety of activities we offer every week. Lastly, the formula holds that only 10 percent of professional development comes from traditional live learning and eLearning events. 

### Instructional Design 101

The Learning & Development, [Field Enablement](/handbook/sales/field-operations/field-enablement/), and [Professional Services](/handbook/customer-success/professional-services-engineering/) teams hosted an Instructional Design 101 session on 2020-10-28 to better understand concepts of adult learning theory and instructional design. Check out the video to learn how to apply instructional design to learning content. 

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Be8oRHp0E84" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Learning Components - Definitions

| Term | Definition | Duration |
| ------ | ------ | ------ |
| Training | The action of teaching a team member a particular skill or behavior | Varies |
| Bite-sized learning | A short course to learn a skill. Bite-sized can be a compentent of training or a separate learning element (i.e. a video, a PDF cheat-sheet, an infographic) | Max 15 minutes, 5 to 10 minute average |
| Self-paced course | Learner has the control over the amount of material they want to consume and the duration of time needed to learn the new information. | Pre-defined by the designer (i.e. hours, days, etc.) |
| Curriculum | A series of learning paths that comprise a course of study on a skill-based topic | Varies |
| Certification | See definition on the [certification page](/learn/certifications/public/#minimum-requirements-of-a-gitlab-certification) | Varies |
| Learning Path | A chosen route taken by a learner through a range of learning activities to build knowledge progressivley | Varies |

This terminology is distinct from our Learning Experience Platform, GitLab Learn, which has it's own [glossary of terms](/handbook/people-group/learning-and-development/gitlab-learn/user/#understanding-content-in-gitlab-learn).

### Learning Delivery Methods - Definitions

| Term | Definition | Duration |
| ------ | ------ | ------ |
| Instructor-Led Training (ILT) | Practice of training and learning material between an instructor and learners. Face-to-face training (classroom training) | Varies |
| Virtual Instructor-Led Training (VILT) | Training that is delivered in a virtual enviornment (i.e. training in Zoom)| 1 to 3 hours |
| E-Learning | Learning based on traditional learning theories which takes place electronically, often via the Internet. | Varies |
| Blended Learning | Training that uses multiple methods to teach the new information to learners. It can includ ILTs, self-paced materials, VILT sessions, etc. | Varies |
| Webinar | A seminar conducted virtually to a large audience | 3+ hours | 
| Workshop | Hands-on or virtual demos, problem solving, tutorials where team members engage in discussion and activity on a particular subject | Varies |



